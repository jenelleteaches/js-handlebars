// 1. Get the name out of localStorage
let name = localStorage.getItem("studentName");
let id = localStorage.getItem("studentId");
let program = localStorage.getItem("studentProgram");

// 2. REQUIRED ERROR HANDLING:  Check if data exists in database
if (name == undefined || id ==  undefined || program == undefined) {
  // 3a. no items found in local storage
  console.log("No information in database.")
  $("#results").html("No information in database.")
}
else {
  console.log("OKAY!")
  // 3b. 'studentName' key exists in localStorage, so output it to UI
  let htmlToAdd = "Name: " + name + "<br>"
  htmlToAdd += "Id: " + id  + "<br>"
  htmlToAdd += "Program: " + program  + "<br>"
  
  $("#results").html(htmlToAdd)
}
