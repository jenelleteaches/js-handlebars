

// This function gets run when the person presses the SAVE button in the UI
function saveButtonPressed() {
  // 1. Get what the person typed in the textbox
  let name = $("#nameInput").val();
  let id = $("#idInput").val();
  let program = $("#programInput").val();

  // 2. OPTIONAL:  Do some error handling to check if the results are blank
  if (name.length == 0 || id.length == 0 || program.length == 0) {
    // user didn't enter anything, so show error message and exit
    $("#results").html("Error: You must enter all information")
    return
  }

  // 3. Add data to local storage
  localStorage.setItem("studentName", name);
  localStorage.setItem("studentId", id);
  localStorage.setItem("studentProgram", program);

  console.log(name + " saved to database");

  // 4. Notify the user that the person was saved to the database
  $("#results").html(name + " saved to database.")
}


// attach the save function to the save button
$("#searchBtn").click(saveButtonPressed)